<?php
// https://medium.com/cafe24-ph-blog/build-your-own-laravel-package-in-10-minutes-using-composer-867e8ef875dd

namespace SAGE\Helpers;


use Illuminate\Support\Str;

class Helpers
{

    /**
     * @param array $data
     * @return string
     * takes an array of data items to format as a properly escaped csv string.  Useful when assembling data
     * that needs to be in csv format but not in an actual file.
     */
    static function filterToCsv(array $data)
    {
        $fp = fopen('php://temp', 'w+');
        fputcsv($fp, $data);
        rewind($fp);
        $csv = stream_get_contents($fp);
        fclose($fp);
        return $csv;
    }

    /**
     * @param array $data
     * @return false|string
     *
     * converts multi-D array to csv
     */
    static function filterAllToCsv(array $data)
    {
        $fp = fopen('php://temp', 'w+');
        foreach ($data as $item) {
            fputcsv($fp, $item);
        }
        rewind($fp);
        $csv = stream_get_contents($fp);
        fclose($fp);
        return $csv;
    }


    /**
     * @param $date
     * @return string
     *
     * Returns a date string
     */
    static function showDate($date)
    {
        if (is_object($date)) {
            return $date->toDateString();
        }
        if (is_string($date)) {
            return Carbon::parse($date)->toDateString();
        }
        return '';
    }


    /**
     * Builds a file path with the appropriate directory separator.
     * @param string $segments,... unlimited number of path segments
     * @return string Path
     */
    static function fileBuildPath(...$segments)
    {
        return join(DIRECTORY_SEPARATOR, $segments);
    }


    /**
     * @param array $data
     * @return array
     *
     * Converts an array of strings to remove all commas and replace with HTML entity
     */
    static function filterArrayComma(array $data)
    {
        foreach ($data as $key => $item) {
            $data[$key] = is_array($item) ? self::filterArrayComma($item) : self::filterStringComma($item);
        }
        return $data;
    }

    /**
     * @param string $data
     * @return string|string[]
     *
     * Replaces a comma in a string with the html entity &comma;
     */
    static function filterStringComma(string $data)
    {
        return str_replace(',', '&comma;', $data);
    }

    /**
     * @param array $data
     *
     * Takes a multi-dimensional array and converts it all to CSV text
     */
    static function makeFullCsv(array $data)
    {
        $result = '';
        foreach ($data as $key => $item) {
            $data[$key] = self::makeCsv($item);
            $result .= self::makeCsv($item) . "\r\n";
        }
        return $result;
    }

    /**
     * @param $data
     * @return string
     *
     * Takes a single dimension array and returns a line of CSV formatted text.
     */
    static function makeCsv($data)
    {
        if (is_object($data)) {
            $data = json_decode(json_encode($data));
        }
        foreach ($data as $key => $value) {
            if (!is_numeric($value)) {
                $data[$key] = '"' . $value . '"';
            }
        }

        return implode(',', $data);
    }

    /**
     * @param array $arr
     * @param SimpleXMLElement $xml
     * @return SimpleXMLElement
     *
     * Converts an array to an XML document
     */
    static function arrayToXml(array $arr, SimpleXMLElement $xml)
    {
        foreach ($arr as $k => $v) {
            $attrArr = array();
            $kArray = explode(' ', $k);
            $tag = array_shift($kArray);

            if (count($kArray) > 0) {
                foreach ($kArray as $attrValue) {
                    $attrArr[] = explode('=', $attrValue);
                }
            }

            if (is_array($v)) {
                if (is_numeric($k)) {
                    array_to_xml($v, $xml);
                } else {
                    $child = $xml->addChild($tag);
                    if (isset($attrArr)) {
                        foreach ($attrArr as $attrArrV) {
                            $child->addAttribute($attrArrV[0], $attrArrV[1]);
                        }
                    }
                    array_to_xml($v, $child);
                }
            } else {
                if (is_object($v)) {
                    if ((get_object_vars($v) === null) || (count(get_object_vars($v)) === false) || (get_object_vars(
                                $v
                            ) == 0)) {
                        $v = '';
                    } else {
                        $v = json_encode($v);
                        $v = ($v == '{}') ? '' : $v;
                    }
                }
                $child = $xml->addChild($tag, $v);
                if (isset($attrArr)) {
                    foreach ($attrArr as $attrArrV) {
                        $child->addAttribute($attrArrV[0], $attrArrV[1]);
                    }
                }
            }
        }
        return $xml;
    }

    /**
     * @param $haystack
     * @param $term
     * @param $color - color to highlight in #999999 format
     * @return string|string[]
     *
     * Does a hightlight of $term in $haystack
     */
    static function highlightString($haystack, $term, $color = "#DA70D6")
    {
        $highlightcolor = $color;
        $occurrences = substr_count(strtolower($haystack), strtolower($term));
        $newstring = $haystack;
        $match = array();

        for ($i = 0; $i < $occurrences; $i++) {
            $match[$i] = stripos($haystack, $term, $i);
            $match[$i] = substr($haystack, $match[$i], strlen($term));
            $newstring = str_replace($match[$i], '[#]' . $match[$i] . '[@]', strip_tags($newstring));
        }

        $newstring = str_replace(
            '[#]',
            '<span style="font-weight: bold; color: ' . $highlightcolor . ';">',
            $newstring
        );
        $newstring = str_replace('[@]', '</span>', $newstring);
        return $newstring;
    }

    /**
     * @param array $data
     * @return array
     *
     * Converts an array of items to snake based keys
     */
    static function toSnakeArray(array $data)
    {
        $newArray = [];
        foreach ($data as $key => $value) {
            if (!ctype_upper($key)) {
                $newArray[Str::snake($key)] = $value;
            } else {
                $newArray[Str::lower($key)] = $value;
            }
        }
        return $newArray;
    }


}
